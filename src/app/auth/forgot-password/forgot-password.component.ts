import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgotForm: FormGroup;
  submitted = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    public toast: ToastrService
  ) { }


  ngOnInit() {
    this.createForm();
  }

  redirectLogin() {
    this.router.navigateByUrl('/login');
  }
  redirectRegister() {
    this.router.navigateByUrl('/internal-user-registration');
  }

  createForm() {

    this.forgotForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });


  }

  get f(): any {
    return this.forgotForm.controls;
  }



  forgotPassword() {
    this.submitted = true;
    if (this.forgotForm.invalid) {
      return;
    }
    this.spinner.show();

    var userName = this.forgotForm.controls['userName'].value;


    this.authService.forgotPassword(userName)
      .pipe(
        finalize(() => {

        })
      )
      .subscribe(baseResponse => {
        if (baseResponse.success) {
          setTimeout(() => {
            this.spinner.hide();
          }, 3000);

          this.toast.success(baseResponse.responseMessage);
        }
        else {
          this.toast.error(baseResponse.responseMessage);
          this.spinner.hide();
        }

      });
  }

}

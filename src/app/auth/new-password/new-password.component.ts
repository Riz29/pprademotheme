import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/internal/operators/finalize';
import { CustomValidationsService } from 'src/app/core/services/custom-validations.service';
import { Password } from '../../core/models/password.model';
import { AuthService } from '../../core/services/auth.service';
import { UserUtilsService } from '../../core/services/user-utils.service';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit {

  newPasswordForm: FormGroup;
  submitted = false;
  password: Password;
  Key: any;
  constructor(
    public fb: FormBuilder,
    private customValidator: CustomValidationsService,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private authService: AuthService,
    private userUtilsService: UserUtilsService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService
  ) {

    this.activeRouter.queryParams.subscribe(params => {
      this.Key = params['key'];
    });


  }

  ngOnInit() {
    this.newPasswordForm = this.fb.group({
      newPassword: ['', Validators.compose([Validators.required, this.customValidator.patternValidator()])],
      confirmPassword: ['', [Validators.required]],
    },
      {
        validator: this.customValidator.MatchPassword('newPassword', 'confirmPassword'),
      });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.newPasswordForm.controls;
  }


  createPassword() {

    this.spinner.show();
    this.submitted = true;
    if (this.newPasswordForm.invalid) {
      return;
    }


    this.password = new Password();

    this.password.Password = this.newPasswordForm.controls['newPassword'].value;
    this.password.Key = this.Key;
    this.authService.createPassword(this.password)
      .pipe(
        finalize(() => {
        })
      )
      .subscribe(baseResponse => {

        if (baseResponse.success) {
          setTimeout(() => {
            this.spinner.hide();
          }, 3000);
          this.router.navigateByUrl('/auth/login');

        }
        else {
          setTimeout(() => {
            this.spinner.hide();
          }, 3000);
          if (baseResponse.errors != null) {
            if (baseResponse.errors.length > 0) {
              for (var i = 0; i < baseResponse.errors.length; i++) {
                this.toast.error(baseResponse.errors[i].errorMessage);
              }
            }
          }
          else {
            this.toast.error(baseResponse.responseMessage);
          }
        }
      }), err => (this.toast.error("Some error occured."));

  }
}



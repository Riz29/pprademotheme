import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalUserRegistrationComponent } from './internal-user-registration.component';

describe('InternalUserRegistrationComponent', () => {




  let component: InternalUserRegistrationComponent;
  let fixture: ComponentFixture<InternalUserRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalUserRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalUserRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

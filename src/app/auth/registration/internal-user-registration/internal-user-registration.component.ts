import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CustomValidationsService } from '../../../core/services/custom-validations.service';
import { DataService } from '../../../core/services/data.service';


@Component({
  selector: 'app-internal-user-registration',
  templateUrl: './internal-user-registration.component.html',
  styleUrls: ['./internal-user-registration.component.scss']
})
export class InternalUserRegistrationComponent implements OnInit {

  OptVerified = false;
  showPersonalInfoForm = false;
  showAddressInfoForm = false;
  showDocumentUploadInfoForm = false;
  showCongratForm = false;
  contactInfoClass = "step active";
  addressClass = "step";
  documentsClass = "step";
  progressBar = "0%";
  internalUserRegistrationForm: FormGroup;
  personalInfoSubmitted = false;
  addressSubmitted = false;
  documentSubmitted = false;
  IsfromMinistry: boolean = false;
  IsfromRegulatory: boolean = false;
  IsfromProcuring: boolean = false;
  message: string;
  subscription: Subscription;
  constructor(
    private fb: FormBuilder,
    private customValidator: CustomValidationsService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
    this.showPersonalInfoForm = true;
    this.showAddressInfoForm = false;
    this.showDocumentUploadInfoForm = false;
    if (this.message == "Min") {
      this.IsfromMinistry = true;
      this.IsfromRegulatory = false;
      this.IsfromProcuring = false;
    }
    else if (this.message == "Pro") {
      this.IsfromMinistry = false;
      this.IsfromRegulatory = false;
      this.IsfromProcuring = true;
    }
    else if (this.message == "Reg") {
      this.IsfromMinistry = false;
      this.IsfromRegulatory = true;
      this.IsfromProcuring = false;
    }
    else if (this.message == "PPMU") {
      this.IsfromMinistry = false;
      this.IsfromRegulatory = false;
      this.IsfromProcuring = false;
    }
    this.createForm();
  }

  cnicMask = this.customValidator.cnicMask();
  phoneMask = this.customValidator.phoneMask();

  // Form Multiple Group

  createForm() {

    this.internalUserRegistrationForm = this.fb.group({
      personalInformation: this.fb.group({
        firstName: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
        lastName: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
        designation: ['', [Validators.required, Validators.maxLength(50)]],
        department: [''],
        userType: ['', Validators.required],
        userRole: ['', Validators.required],
        grade: ['', [Validators.required, Validators.maxLength(30)]],
        procurementAgency: ['', Validators.required],
        wing: ['', Validators.required],
        dob: ['', Validators.required],
        email: ['', [Validators.required, Validators.maxLength(100)]],
        cnic: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        phoneService: ['', Validators.required]
      }),

      addressInformation: this.fb.group({
        house: ['', [Validators.required, Validators.maxLength(6)]],
        street: ['', [Validators.required, Validators.maxLength(6)]],
        block: ['', [Validators.required, Validators.maxLength(6)]],
        addressLine2: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(200)]],
        city: ['', Validators.required],
        zipCode: ['', [Validators.required, Validators.maxLength(10)]],
        country: ['', Validators.required],
        province: ['', Validators.required],
        district: ['', Validators.required],
        tehsil: ['', Validators.required]
      }),

      uploadDocumentInformation: this.fb.group({
        documentType: ['', Validators.required],
        documentName: ['', Validators.required]
      })

    });


  }

  get fp() { return (<FormGroup>this.internalUserRegistrationForm.get('personalInformation')).controls; }
  get fa() { return (<FormGroup>this.internalUserRegistrationForm.get('addressInformation')).controls; }
  get fd() { return (<FormGroup>this.internalUserRegistrationForm.get('uploadDocumentInformation')).controls; }


  get personalInformation() {
    return this.internalUserRegistrationForm.get('personalInformation');
  }

  get addressInformation() {
    return this.internalUserRegistrationForm.get('addressInformation');
  }

  get uploadDocumentInformation() {
    return this.internalUserRegistrationForm.get('uploadDocumentInformation');
  }



  address() {
    this.personalInfoSubmitted = true;
    if (this.internalUserRegistrationForm.controls.personalInformation.invalid) {
      return;
    }
    this.showAddressInfoForm = true;
    this.showPersonalInfoForm = false;
    this.contactInfoClass = "step completed";
    this.addressClass = "step active";
    this.progressBar = "33%";
  }
  uploadDocuments() {
    this.addressSubmitted = true;
    if (this.internalUserRegistrationForm.controls.addressInformation.invalid) {
      return;
    }
    this.showDocumentUploadInfoForm = true;
    this.showAddressInfoForm = false;
    this.showPersonalInfoForm = false;
    this.addressClass = "step completed";
    this.documentsClass = "step active";
    this.progressBar = "66%";

  }
  previousUploadDocuments() {
    this.showAddressInfoForm = true;
    this.showPersonalInfoForm = false;
    this.showDocumentUploadInfoForm = false;
  }


  previousAddress() {
    this.showAddressInfoForm = false;
    this.showPersonalInfoForm = true;
    this.showDocumentUploadInfoForm = false;
  }


  nextSave() {
    this.documentSubmitted = true;
    if (this.internalUserRegistrationForm.controls.uploadDocumentInformation.invalid) {
      return;
    }
    this.showDocumentUploadInfoForm = false;
    this.showCongratForm = true;
    let userName = this.internalUserRegistrationForm.controls['personalInformation'].value.firstName;
    this.documentsClass = "step completed";
    this.progressBar = "100%";
  }


  generateOtp() {

  }

  validateOtp() {

  }

}

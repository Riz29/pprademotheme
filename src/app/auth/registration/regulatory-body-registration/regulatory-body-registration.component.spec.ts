import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegulatoryBodyRegistrationComponent } from './regulatory-body-registration.component';

describe('RegulatoryBodyRegistrationComponent', () => {
  let component: RegulatoryBodyRegistrationComponent;
  let fixture: ComponentFixture<RegulatoryBodyRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegulatoryBodyRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegulatoryBodyRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

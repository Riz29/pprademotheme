import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-regulatory-body-registration',
  templateUrl: './regulatory-body-registration.component.html',
  styleUrls: ['./regulatory-body-registration.component.scss']
})
export class RegulatoryBodyRegistrationComponent implements OnInit {

  regulatoryBodyRegistrationForm: FormGroup;
  InfoSubmitted = false;


  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.createForm();
  }


  createForm() {

    this.regulatoryBodyRegistrationForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(6)]],
      code: ['', [Validators.required, Validators.minLength(6)]],
      province: ['', Validators.required],
      website: ['', Validators.required],
      file: ['']
    });
  }



  get f(): any {
    return this.regulatoryBodyRegistrationForm.controls;
  }




  save() {
    
    this.InfoSubmitted = true;
    if (this.regulatoryBodyRegistrationForm.invalid) {
      return;
    }


  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalUserRegistrationByAdminComponent } from './internal-user-registration-by-admin.component';

describe('InternalUserRegistrationByAdminComponent', () => {
  let component: InternalUserRegistrationByAdminComponent;
  let fixture: ComponentFixture<InternalUserRegistrationByAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalUserRegistrationByAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalUserRegistrationByAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

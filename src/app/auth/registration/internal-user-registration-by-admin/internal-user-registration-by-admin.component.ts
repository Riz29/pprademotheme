import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-internal-user-registration-by-admin',
  templateUrl: './internal-user-registration-by-admin.component.html',
  styleUrls: ['./internal-user-registration-by-admin.component.scss']
})
export class InternalUserRegistrationByAdminComponent implements OnInit {

  showPersonalInfoForm = false;
  showAddressInfoForm = false;
  showDocumentUploadInfoForm = false;
  contactInfoClass = "step active";
  addressClass = "step";
  documentsClass = "step";
  progressBar = "0%";
  internalUserRegistrationForm: FormGroup;
  personalInfoSubmitted = false;
  addressSubmitted = false;
  documentSubmitted = false;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.showPersonalInfoForm = true;
    this.showAddressInfoForm = false;
    this.showDocumentUploadInfoForm = false;
    this.createForm();
  }


  // Form Multiple Group

  createForm() {

    this.internalUserRegistrationForm = this.fb.group({
      personalInformation: this.fb.group({
        firstName: ['', [Validators.required, Validators.minLength(6)]],
        lastName: ['', [Validators.required, Validators.minLength(6)]],
        designation: ['', Validators.required],
        department: ['', Validators.required],
        userType: ['', Validators.required],
        userRole: ['', Validators.required],
        grade: ['', Validators.required],
        procurementAgency: ['', Validators.required],
        wing: ['', Validators.required],
        dob: ['', Validators.required],
        email: ['', Validators.required],
        cnic: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        phoneService: ['', Validators.required]
      }),

      addressInformation: this.fb.group({
        addressLine1: ['', Validators.required],
        addressLine2: ['', Validators.required],
        city: ['', Validators.required],
        zipCode: ['', Validators.required],
        country: ['', Validators.required],
        province: ['', Validators.required],
        district: ['', Validators.required],
        tehsil: ['', Validators.required]
      }),

      uploadDocumentInformation: this.fb.group({
        documentType: ['', Validators.required],
        documentName: ['', Validators.required]
      })

    });


  }

  get fp() { return (<FormGroup>this.internalUserRegistrationForm.get('personalInformation')).controls; }
  get fa() { return (<FormGroup>this.internalUserRegistrationForm.get('addressInformation')).controls; }
  get fd() { return (<FormGroup>this.internalUserRegistrationForm.get('uploadDocumentInformation')).controls; }


  get personalInformation() {
    return this.internalUserRegistrationForm.get('personalInformation');
  }

  get addressInformation() {
    return this.internalUserRegistrationForm.get('addressInformation');
  }

  get uploadDocumentInformation() {
    return this.internalUserRegistrationForm.get('uploadDocumentInformation');
  }

  address() {
    

    //this.personalInfoSubmitted = true;
    //if (this.internalUserRegistrationForm.controls.personalInformation.invalid) {
    //  return;
    //}

    
    this.showAddressInfoForm = true;
    this.showPersonalInfoForm = false;
    this.contactInfoClass = "step completed";
    this.addressClass = "step active";
    this.progressBar = "33%";


  }


  uploadDocuments() {

    //this.addressSubmitted = true;
    //if (this.internalUserRegistrationForm.controls.addressInformation.invalid) {
    //  return;
    //}

    this.showDocumentUploadInfoForm = true;
    this.showAddressInfoForm = false;
    this.showPersonalInfoForm = false;
    this.addressClass = "step completed";
    this.documentsClass = "step active";
    this.progressBar = "66%";

  }


  previousUploadDocuments() {
    this.showAddressInfoForm = true;
    this.showPersonalInfoForm = false;
    this.showDocumentUploadInfoForm = false;
  }



  previousAddress() {
    this.showAddressInfoForm = false;
    this.showPersonalInfoForm = true;
    this.showDocumentUploadInfoForm = false;
  }



  nextSave() {

    this.documentSubmitted = true;
    if (this.internalUserRegistrationForm.controls.uploadDocumentInformation.invalid) {
      return;
    }

    
    let userName = this.internalUserRegistrationForm.controls['personalInformation'].value.firstName;
    this.documentsClass = "step completed";
    this.progressBar = "100%";
  }

}

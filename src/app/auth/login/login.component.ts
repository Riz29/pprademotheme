import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UserInformation } from '../../core/models/user-information.model';
import { AuthService } from '../../core/services/auth.service';
import { DataService } from '../../core/services/data.service';
import { UserUtilsService } from '../../core/services/user-utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  userInfo: UserInformation;
  submitted = false;
  subscription: Subscription;
  message: string;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private userUtilsService: UserUtilsService,
    private spinner: NgxSpinnerService,
    public data: DataService,
    public toast: ToastrService
  ) { }


  ngOnInit() {
    sessionStorage.removeItem("Type");
    this.createForm();
  }




  createForm() {

    this.loginForm = this.fb.group({
      userName: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]

    });


  }

  //hasError(controlName: string, errorName: string): boolean {
  //  return this.loginForm.controls[controlName].hasError(errorName);
  //}


  get f(): any {
    return this.loginForm.controls;
  }


  registrationstype(Type: any) {
    if (Type == "Reg") {
      this.data.changeMessage("Reg");
      sessionStorage.setItem("Type", "Reg");
      this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
      this.router.navigateByUrl('/internal-user-registration');
    }
    else if (Type == "Min") {
      this.data.changeMessage("Min");
      sessionStorage.setItem("Type", "Min");
      this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
      this.router.navigateByUrl('/internal-user-registration');
    }
    else if (Type == "Pro") {
      this.data.changeMessage("Pro");
      sessionStorage.setItem("Type", "Pro");
      this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
      this.router.navigateByUrl('/internal-user-registration');
    }
    else if (Type == "PPMU") {
      this.data.changeMessage("PPMU");
      sessionStorage.setItem("Type", "PPMU");
      this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
      this.router.navigateByUrl('/internal-user-registration');
    }

  }

  login() {
    debugger
    this.router.navigateByUrl('/dashboard');
    //this.submitted = true;
    // if (this.loginForm.invalid) {
    //   return;
    // }
  //   this.spinner.show();
  //   this.userInfo = new UserInformation();
  //   this.userInfo = Object.assign(this.userInfo, this.loginForm.value);
  //   this.authService.login(this.userInfo)
  //     .pipe(
  //       finalize(() => {
  //       })
  //     )
  //     .subscribe(baseResponse => {
  //       if (baseResponse.success) {
  //         setTimeout(() => {
  //           this.spinner.hide();
  //         }, 3000);
  //         this.userUtilsService.setUserDetails(baseResponse);
  //         this.router.navigateByUrl('/dashboard');

  //       }
  //       else {
  //         setTimeout(() => {
  //           this.spinner.hide();
  //         }, 1000);
  //         if (baseResponse.errors.length != 0) {
  //           if (baseResponse.errors.length > 0) {
  //             for (var i = 0; i < baseResponse.errors.length; i++) {
  //               this.toast.error(baseResponse.errors[i].errorMessage);
  //             }
  //           }
  //         }
  //         else {
  //           this.toast.error(baseResponse.responseMessage);
  //         }

  //       }
  //     }),
  //     err => (this.toast.error("Some error occured."));

   }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserInformation } from '../../core/models/user-information.model';
import { UserUtilsService } from '../../core/services/user-utils.service';

@Component({
  selector: 'app-super-admin-login',
  templateUrl: './super-admin-login.component.html',
  styleUrls: ['./super-admin-login.component.scss']
})
export class SuperAdminLoginComponent implements OnInit {


  loginForm: FormGroup;
  userInfo: UserInformation;
  submitted = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private userUtilsService: UserUtilsService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService
  ) { }


  ngOnInit() {
    this.createForm();
  }




  createForm() {

    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]

    });


  }

  //hasError(controlName: string, errorName: string): boolean {
  //  return this.loginForm.controls[controlName].hasError(errorName);
  //}


  get f(): any {
    return this.loginForm.controls;
  }



  login() {

    this.spinner.show();
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }


    this.userInfo = new UserInformation();
    this.userInfo.Username = this.loginForm.controls["email"].value;
    this.userInfo.Password = this.loginForm.controls["password"].value;


    this.authService.login(this.userInfo)
      .pipe(
        finalize(() => {
        })
      )
      .subscribe(baseResponse => {

        if (baseResponse.success) {
          setTimeout(() => {
            this.spinner.hide();
          }, 3000);
          this.userUtilsService.setUserDetails(baseResponse.data);
          this.router.navigateByUrl('/dashboard');

        }
        else {
          setTimeout(() => {
            this.spinner.hide();
          }, 3000);
          if (baseResponse.errors != null) {
            if (baseResponse.errors.length > 0) {
              for (var i = 0; i < baseResponse.errors.length; i++) {
                this.toast.error(baseResponse.errors[i].errorMessage);
              }
            }
          }
          else {
            this.toast.error(baseResponse.responseMessage);
          }
        }
      }), err => (this.toast.error("Some error occured."));

  }

}


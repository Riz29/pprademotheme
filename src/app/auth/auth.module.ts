import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InternalUserRegistrationComponent } from './registration/internal-user-registration/internal-user-registration.component';
import { RegulatoryBodyRegistrationComponent } from './registration/regulatory-body-registration/regulatory-body-registration.component';
import { AuthComponent } from './auth.component';
import { RouterModule, Routes } from '@angular/router';
import { InternalUserRegistrationByAdminComponent } from './registration/internal-user-registration-by-admin/internal-user-registration-by-admin.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { IntercepterService } from '../core/services/intercepter.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NewPasswordComponent } from './new-password/new-password.component';
import { ThemeModule } from '../theme/theme.module';
import { SuperAdminLoginComponent } from './super-admin-login/super-admin-login.component';
import { TextMaskModule } from 'angular2-text-mask';
import { SliderComponent } from './slider/slider.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'super-admin-login',
        component: SuperAdminLoginComponent,
      },
      {
        path: 'internal-user-registration',
        component: InternalUserRegistrationComponent,
      },
      {
        path: 'internal-user-registration-by-admin',
        component: InternalUserRegistrationByAdminComponent,
      },
      {
        path: 'regulatory-body-registration',
        component: RegulatoryBodyRegistrationComponent,
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
      },
      {
        path: 'create-password',
        component: NewPasswordComponent,
      }
    ]
  }]

@NgModule({
  declarations: [LoginComponent, InternalUserRegistrationComponent, RegulatoryBodyRegistrationComponent, AuthComponent,
    InternalUserRegistrationByAdminComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    NewPasswordComponent,
    SliderComponent,
    SuperAdminLoginComponent],
  providers: [
    IntercepterService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: IntercepterService,
      multi: true
    },
  ],
  exports: [],
  imports: [
    CommonModule,
    FormsModule,
    ThemeModule,
    TextMaskModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ]
})
export class AuthModule { }

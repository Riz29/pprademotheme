import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { CustomValidationsService } from 'src/app/core/services/custom-validations.service';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  submitted = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private customValidator: CustomValidationsService,
  ) { }


  ngOnInit() {
    this.createForm();
  }




  createForm() {

    this.changePasswordForm = this.fb.group({
      oldPassword: ['', [Validators.required, Validators.minLength(8)]],
    //   newPassword: ['', [Validators.required, Validators.minLength(8)]],
    //   confirmPassword: ['', [Validators.required, Validators.minLength(8)]]
    // });
      newPassword: ['', Validators.compose([Validators.required, this.customValidator.patternValidator()])],
      confirmPassword: ['', [Validators.required]],
    },
    {
        validator: this.customValidator.MatchPassword('newPassword', 'confirmPassword'),
    });
  }

  hasError(controlName: string, errorName: string): boolean {
    return this.changePasswordForm.controls[controlName].hasError(errorName);
  }


  get f(): {[key:string] : AbstractControl} {
    return this.changePasswordForm.controls;
  }

 

  login() {
    
    this.submitted = true;
    if (this.changePasswordForm.invalid) {
      return;
    }

    // this.authService.add()
    //   .pipe(
    //     finalize(() => {

    //     })
    //   )
    //   .subscribe(baseResponse => {
    //     if (baseResponse.isSuccess) {
    //       this.router.navigateByUrl('/dashboard');
    //       
    //       alert('Login successfully!');
    //     }
    //     else
    //       alert('form fields are validated successfully!');
    //   });
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/helpers/auth.guard';
import { BaseComponent } from './theme/base/base.component';

const routes: Routes = [
  {
    path: 'auth', loadChildren: () => import('src/app/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    runGuardsAndResolvers: "always",
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('src/app/dashboard/dashboard.module').then(m => m.DashboardModule)
      }
      // {
      //   path: 'usermanagement',
      //   loadChildren: () => import('src/app/user-management/user-management.module').then(m => m.UserManagementModule)
      // }
    ]

  },
  { path: '**', redirectTo: 'error/403', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: "reload" })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

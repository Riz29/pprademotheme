import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { IntercepterService } from '../core/services/intercepter.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ThemeModule } from '../theme/theme.module';



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    ThemeModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent
      }
    ]),
  ],
  providers: [
    IntercepterService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: IntercepterService,
      multi: true
    },
  ],
})
export class DashboardModule { }

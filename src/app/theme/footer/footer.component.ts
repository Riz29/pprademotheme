import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.loadScript("assets/adminlte/dist/js/demo.js");
    setTimeout(() => {
      this.loadScript("assets/adminlte/dist/js/pages/dashboard.js");
    }, 1000);
  }
  public loadScript(url: string) {
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
  }
}

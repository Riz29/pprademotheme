import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styles: [
  ]
})
export class TopnavComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
  }
  redirectToHome() {
    this.router.navigateByUrl('dashboard/home');
  }
  redirectToAbout() {
    this.router.navigateByUrl('dashboard/about');
  }
  logMeOut() {
    this.router.navigateByUrl('login');
  }
}

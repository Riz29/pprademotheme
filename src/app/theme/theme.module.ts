import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base/base.component';
import { RouterModule } from '@angular/router';
import { AsidenavComponent } from './asidenav/asidenav.component';
import { TopnavComponent } from './topnav/topnav.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [BaseComponent, AsidenavComponent, TopnavComponent, FooterComponent],
  exports: [
    BaseComponent,
    AsidenavComponent, 
    TopnavComponent, 
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class ThemeModule { }

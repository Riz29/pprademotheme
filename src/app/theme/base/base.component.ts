import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var Base = function () {
      var global = {
        tooltipOptions: {
          placement: "right"
        },
        menuClass: ".c-menu"
      };

      var menuChangeActive = function menuChangeActive(el) {
        var hasSubmenu = $(el).hasClass("has-submenu");
        if ($(el).hasClass("is-active")) {
          $(el).removeClass("is-active")
          $(el).find("ul").hide('Slow');
        }
        else {
          $(global.menuClass + " .is-active").removeClass("is-active");
          $(el).addClass("is-active");

          if (hasSubmenu) {
            $(el).find("ul").slideDown();
          }
        }
      };

      var sidebarChangeWidth = function sidebarChangeWidth() {
        var $menuItemsTitle = $("li .menu-item__title");
        $("body").toggleClass("sidebar-is-reduced sidebar-is-expanded");
        $(".hamburger-toggle").toggleClass("is-opened");

        if ($("div").hasClass("sidebar-is-reduced")) {
          if ($("li.c-menu__item").hasClass("has-submenu")) {
            $("li.c-menu__item").find("ul").hide('Slow');
            $("li.c-menu__item").removeClass("is-active")
          }
        }
      };

      return {
        init: function init() {
          $(".js-hamburger").on("click", sidebarChangeWidth);

          $(".js-menu li").on("click", function (e) {
            menuChangeActive(e.currentTarget);
          });

          /*$('[data-toggle="tooltip"]').tooltip(global.tooltipOptions);*/
        }
      };
    }();

    Base.init();
  }

}

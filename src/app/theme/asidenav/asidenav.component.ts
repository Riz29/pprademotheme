import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-asidenav',
  templateUrl: './asidenav.component.html',
  styles: [
  ]
})
export class AsidenavComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
  }
  redirectToHome() {
    this.router.navigateByUrl('dashboard/home');
  }
}

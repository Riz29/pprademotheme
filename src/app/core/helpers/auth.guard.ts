import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserUtilsService } from '../services/user-utils.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  private subject: BehaviorSubject<boolean>;
  constructor(private router: Router,
    private userUtilsService: UserUtilsService, ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    

    

    var tokenExpired = this.userUtilsService.isTokenExpired();
    this.subject = new BehaviorSubject<boolean>(!tokenExpired);

    if (tokenExpired) {
      
      this.userUtilsService.removeUserDetails();
      this.router.navigateByUrl('/auth/login');
      return;
    }

    var isValidUrl = this.userUtilsService.isValidUrl(state.url);

    if (!isValidUrl) {

      var menu = this.userUtilsService.getUserMenu();
      if (menu == null || menu == undefined) {
        this.userUtilsService.removeUserDetails();
        this.router.navigateByUrl('/auth/login');
        return;
      }
      this.router.navigateByUrl('/auth/unauthorize');
      return;
    }


    return this.subject.asObservable();
  }

}

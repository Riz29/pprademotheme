
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { BaseResponse } from '../models/base-response.model';
import { ministrymodel } from '../models/ministry-model';
import { PaginationModel } from '../models/pagination-model';
import { ProcuringAgencyModel } from '../models/procuring-agency-model';
import { RegulatoryBody } from '../models/regulatory-body';
import { RequestMain } from '../models/request.model';
import { HttpUtilsService } from './http-utils.service';
import { UserUtilsService } from './user-utils.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {
  request: RequestMain;
  backEndAPI: string = "/api/user/";
  //backEndAPI: string = "/v1/api/userManagement/";
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService, private userUtilsService: UserUtilsService) { }

  // Regulatory-body-Crud
  addRegulatoryBody(r: RegulatoryBody, file: any): Observable<BaseResponse> {

    var auth = this.userUtilsService.getUserDetails();

    const formData = new FormData();
    formData.append('RegulatoryBody.code', r.code);
    formData.append('RegulatoryBody.name', r.name);
    formData.append('RegulatoryBody.province', r.provinceId);
    formData.append('RegulatoryBody.webSiteURL', r.webSiteURL);
    formData.append('RegulatoryBody.files', file);
    formData.append('auth.access_token', auth.access_token);
    formData.append('auth.refresh_token', auth.refresh_token);
    formData.append('auth.expires_in', auth.expires_in);
    formData.append('schemaName', auth.schemaName);
    formData.append('userID', auth.userInfo.userID);
    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/saveregulatory`, formData,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  updateRegulatoryBody(): Observable<BaseResponse> {

    var req = JSON.stringify('');

    return this.http.post(`${environment.apiUrl}/User/Add`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  deleteRegulatoryBody(): Observable<BaseResponse> {

    var req = JSON.stringify('');

    return this.http.post(`${environment.apiUrl}/User/Add`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getAllRegulatoryBodies(): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.userID = auth.userInfo.userID;
    if (auth.userInfo.userTypeID == 1) {
      this.request.schemaName = "dbo"
    }
    else {
      this.request.schemaName = auth.schemaName;
    }


    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getallregulatories`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getRegulatoryBodiesInfo(r: PaginationModel): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    if (auth.userInfo.userTypeID == 1) {
      this.request.schemaName = "dbo"
    }
    else {
      this.request.schemaName = auth.schemaName;
    }
    this.request.userID = auth.userInfo.userID;
    this.request.pagination = r;
    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getallregulatoryInfo`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  // Internal-User-Crud
  internalUserAdd(r: RequestMain): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.user = r.user;
    this.request.address = r.address;
    this.request.profile = r.profile;
    this.request.userID = auth.userInfo.userID;
    if (auth.userInfo.userTypeID == 1) {
      this.request.schemaName = "dbo"
    }
    else {
      this.request.schemaName = auth.schemaName;
    }
    this.request.assignUserRole = r.assignUserRole;
    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/saveUser`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getAllUsers(r: RequestMain): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    if (auth.userInfo.userTypeID == 1) {
      this.request.schemaName = "dbo"
    }
    else {
      this.request.schemaName = auth.schemaName;
    }
    this.request.userID = auth.userInfo.userID;
    this.request.pagination = r.pagination;
    this.request.businessUnit = r.businessUnit;
    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getallUsers`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getAllRegulatorybodyUsers(r: RequestMain): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    if (auth.userInfo.userTypeID == 1) {
      this.request.schemaName = "dbo"
    }
    else {
      this.request.schemaName = auth.schemaName;
    }
    this.request.userID = auth.userInfo.userID;
    this.request.pagination = r.pagination;
    this.request.businessUnit = r.businessUnit;
    var req = JSON.stringify(this.request);
    debugger;
    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getallRegulatorybodyUsers`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(catchError(this.errorHandler),
        map((res: BaseResponse) => res));
  }
  getAllMinistryUsers(r: RequestMain): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.schemaName = auth.schemaName;
    this.request.userID = auth.userInfo.userID;
    this.request.pagination = r.pagination;
    this.request.businessUnit = r.businessUnit;
    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getallMinistryUsers`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  GetAllProcuringAgencyUsers(r: RequestMain): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.userID = auth.userInfo.userID;
    if (auth.userInfo.userID == 1) {
      this.request.schemaName = "dbo";
    }
    else {
      this.request.schemaName = auth.schemaName;
    }

    this.request.pagination = r.pagination;
    this.request.businessUnit = r.businessUnit;
    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/GetAllProcuringAgencyUsers`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }



  uploadDocuments() {

    this.request = new RequestMain();

    const formData = new FormData();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;

    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/uploadDocument`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }

  // Ministries-Crud
  AddMinistry(r: ministrymodel, Minschema: string): Observable<BaseResponse> {

    this.request = new RequestMain();

    const formData = new FormData();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.schemaName = Minschema;
    this.request.userID = auth.userInfo.userID;
    this.request.Ministry = r;

    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/saveministry`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  UpdateMinistry(r: ministrymodel): Observable<BaseResponse> {

    this.request = new RequestMain();

    const formData = new FormData();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.Ministry = r;

    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/updateministry`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getAllMinistries(r: PaginationModel, Minschema: string): Observable<BaseResponse> {
    this.request = new RequestMain();
    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.schemaName = Minschema;
    this.request.userID = auth.userInfo.userID;
    this.request.pagination = r;
    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getallministries`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getMinistriesByRegulatory(): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;

    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getMinistriesByRegulatory`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }

  // Procuring-Agency-Crud
  saveProcuringAgency(r: ProcuringAgencyModel, Minschema: string): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.schemaName = Minschema;
    this.request.userID = auth.userInfo.userID;
    this.request.ProcuringAgency = r;

    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/saveProcuringAgency`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  updateProcuringAgency(r: ProcuringAgencyModel): Observable<BaseResponse> {

    this.request = new RequestMain();

    const formData = new FormData();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.ProcuringAgency = r;

    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/updateProcuringAgency`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getAllProcuringAgencies(Minschema: string): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;
    this.request.schemaName = Minschema;
    this.request.userID = auth.userInfo.userID;
    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getAllProcuringAgencies`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  getAllProcuringAgenciesByMinistry(): Observable<BaseResponse> {

    this.request = new RequestMain();

    var auth = this.userUtilsService.getUserDetails();
    this.request.auth = auth;

    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getallregulatories`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server error.");
  }

}

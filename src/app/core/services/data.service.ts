import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private messageSource = new BehaviorSubject('');
  private idSource = new BehaviorSubject(0);
  private minSource = new BehaviorSubject("");
  private ministrySource = new BehaviorSubject(0);
  currentMinistryId = this.ministrySource.asObservable();
  private procuringSource = new BehaviorSubject(0);
  currentProcuringId = this.procuringSource.asObservable();
  currentId = this.idSource.asObservable();
  currentMessage = this.messageSource.asObservable();
  currentMin = this.minSource.asObservable();

  message: string;
  id: number;
  min: string;
  ministryId: number;
  procuringId: number;
  constructor() { }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  changeId(id: number) {
    this.idSource.next(id)
  }
  changeMin(min: string) {
    this.minSource.next(min)
  }

  changeMinistryId(ministryId: number) {
    this.ministrySource.next(ministryId)
  }
  changeProcuringId(procuringId: number) {
    this.procuringSource.next(procuringId)
  }
}

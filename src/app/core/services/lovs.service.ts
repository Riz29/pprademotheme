import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpUtilsService } from './http-utils.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { BaseResponse } from '../models/base-response.model';
import { LovRequest } from '../models/lov-request.model';
import { LovModel } from '../models/lov-model.model';
import { UserUtilsService } from './user-utils.service';
import { RequestMain } from '../models/request.model';


@Injectable({
  providedIn: 'root'
})
export class LovsService {
  request: RequestMain;

  constructor(private http: HttpClient, private httpUtils: HttpUtilsService, private userUtilsService: UserUtilsService) { }


  getLovs(request: LovModel): Observable<BaseResponse> {
    
    this.request = new RequestMain();
    var auth = this.userUtilsService.getUserDetails();
    this.request.Lov = request;
    this.request.auth = auth;
    this.request.userID = auth.userInfo.userID;
    if (auth.userInfo.userID == 1) {
      this.request.schemaName = "dbo";
    }
    else {
      this.request.schemaName = auth.schemaName;
    }


    var req = JSON.stringify(this.request);

    return this.http.post(`${environment.apiUrl}/v1/api/userManagement/getLov`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
}

import { Injectable } from '@angular/core';

import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { UserUtilsService } from './user-utils.service';

@Injectable({
  providedIn: 'root'
})
export class IntercepterService implements HttpInterceptor {

  constructor(private router: Router, private authService: AuthService, private userUtilsService: UserUtilsService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /*  throw new Error('Method not implemented.');*/
    if (!req.url.includes("/v1/api/userManagement/saveregulatory")) {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        }
      });


    }

    return next.handle(req).pipe(
      tap(
        event => {
          if (event instanceof HttpResponse) {
            if (event.body.tokenHasExpired) {
              this.authService.logout();
              this.router.navigate(['/login']);
            }
          }
        },
        error => {
          debugger
          console.error(error.status);
          console.error(error.message);
        }
      )
    );

  }
}

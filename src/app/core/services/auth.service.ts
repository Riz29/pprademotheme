import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { BaseResponse } from '../models/base-response.model';
import { UserInformation } from '../models/user-information.model';
import { HttpUtilsService } from './http-utils.service';
import { Router } from '@angular/router';
import { UserUtilsService } from './user-utils.service';
import { RequestMain } from '../models/request.model';
import { Password } from '../models/password.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  request: RequestMain;
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService, private userUtilsService: UserUtilsService, private router: Router) { }

  login(r: UserInformation): Observable<BaseResponse> {
    this.request = new RequestMain();
    this.request.user = r;
    var req = JSON.stringify(this.request);
    return this.http.post(`${environment.apiUrl}/v1/Api/userManagement/OAuth/Login`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }


  createPassword(r: Password): Observable<BaseResponse> {

    this.request = new RequestMain();
    this.request.Key = r.Key;
    this.request.Password = r.Password;
    var req = JSON.stringify(this.request);
    return this.http.post(`${environment.apiUrl}/v1/Api/userManagement/setPassword`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }
  forgotPassword(r: any): Observable<BaseResponse> {

    this.request = new RequestMain();
    this.request.Username = r;
    var req = JSON.stringify(this.request);
    return this.http.post(`${environment.apiUrl}/v1/Api/userManagement/forgotPassword`, req,
      { headers: this.httpUtils.getHTTPHeaders() }).pipe(
        map((res: BaseResponse) => res));
  }


  //  const userToken = localStorage.getItem(environment.authTokenKey);
  //  const httpHeaders = new HttpHeaders();
  //  httpHeaders.set('Authorization', 'Bearer ' + userToken);
  //  return this.http.get<User>(API_USERS_URL, { headers: httpHeaders });

  logout() {
    this.userUtilsService.removeUserDetails();
    sessionStorage.removeItem("ministryId");
    sessionStorage.removeItem("procuringId");
    sessionStorage.removeItem("minSchemaName");
    sessionStorage.removeItem("regBodyId");
    this.router.navigate(['/login']);
  }



}

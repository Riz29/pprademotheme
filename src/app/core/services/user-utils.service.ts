import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { LoginResponse } from '../models/login-response.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserUtilsService {

  loginResponseSet: LoginResponse;
  loginResponseGet: LoginResponse;

  constructor(private cookies: CookieService) {

  }

  public getUserDetails(): any {

    var data = this.cookies.get(environment.loignResponseKey);
    if (data != "") {
      this.loginResponseGet = JSON.parse(data);
      return this.loginResponseGet;
    }

    return null;
  }

  public setUserDetails(response: any) {
  
    this.loginResponseSet = new LoginResponse();
    this.loginResponseSet.access_token = response.auth.access_token;
    this.loginResponseSet.refresh_token = response.auth.refresh_token;
    this.loginResponseSet.scope = response.auth.scope;
    this.loginResponseSet.token_type = response.auth.token_type;
    this.loginResponseSet.expires_in = response.auth.expires_in;
    this.loginResponseSet.error = response.auth.error;
    this.loginResponseSet.error_description = response.auth.error_description;
    this.loginResponseSet.tokenHasExpired = response.auth.tokenHasExpired;
    var responseData = JSON.parse(response.data);
    this.loginResponseSet.userInfo = responseData.userInfo;
    this.loginResponseSet.schemaName = responseData.schemaName;
    this.loginResponseSet.userProfile = responseData.userProfile;
    this.loginResponseSet.permissions = responseData.permissions;
    this.cookies.set(environment.loignResponseKey, JSON.stringify(this.loginResponseSet), 1);
  }


  public setUserMenu() {

  }

  public getUserMenu() {
  }

  public removeUserDetails(): any {
    this.cookies.deleteAll(environment.loignResponseKey);
  }


  public getUserActivities() {

  }

  public getActivity() {

  }


  public isValidUrl(url: string): boolean {

    return true;
  }


  public isTokenExpired() {
    return false;
    // this.getUserDetails();
    // if (this.loginResponseGet) {
    //   if (this.loginResponseGet.access_token != null) {
    //     return false;
    //   }
    // }
    // return true;
  }

}

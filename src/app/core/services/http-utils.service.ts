import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { UserUtilsService } from './user-utils.service';
import { User } from 'oidc-client';

@Injectable({
  providedIn: 'root'
})
export class HttpUtilsService {

  constructor(private userUtilsService: UserUtilsService) {

  }

  getHTTPHeaders(): HttpHeaders {

    ;
    var userData = this.userUtilsService.getUserDetails();

    if (userData != null) {

      const result = new HttpHeaders();
      result.set('Content-Type', 'application/json');
      result.set('Authorization', 'Bearer ' + userData.accessToken);
      return result;
    }
    else {

      const result = new HttpHeaders();
      result.set('Content-Type', 'application/json');
      return result;
    }

  }

}

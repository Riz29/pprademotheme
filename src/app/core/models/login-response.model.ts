export class LoginResponse {
  access_token: string;
  refresh_token: string;
  scope: string;
  token_type: string;
  expires_in: number;
  error_description: string;
  error: string;
  tokenHasExpired: boolean;
  userInfo: object;
  userProfile: object;
  schemaName: string;
  permissions: any[];
}

import { BaseRequest } from "./base-request.model";

export class RequestMain extends BaseRequest {
  Lov: object;
  user: object;
  profile: object;
  address: object;
  regulatoryBody: object;
  Ministry: object;
  ProcuringAgency: object;
  pagination: object;
  businessUnit: object;
  assignUserRole: object;
  otp: object;
  Key: string;
  Password: string;
  Username: string;
  properties: any[];
}

export class BaseResponse {
  responseCode: string;
  responseMessage: string;
  success: boolean;
  data: object;
  tokenHasExpired: boolean;
  errors: any;
}


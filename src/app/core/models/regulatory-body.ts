export class RegulatoryBody {
  code: string;
  name: string;
  webSiteURL: string;
  provinceId: string;
}

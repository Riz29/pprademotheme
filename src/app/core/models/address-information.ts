export class addressinformation {
  houseNo: string;
  streetNo: string;
  blockNo: string;
  addressLine2: string;
  zipOrPostalCode: string;
  country: string;
  provinceID: string;
  divisionID: string;
  districtID: string;
  tehsilID: string
}

export class BaseRequest {
  Schema: string;
  auth: object;
  schemaName: string;
  userID: number
}

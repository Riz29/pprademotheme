export class UserPersonelInformation {
  id: number;
  userID: number;
  firstName: string;
  lastName: string;
  grade: string;
  cnic: string;
  email: string;
  dob: Date;
  address: string = "";
  designation: string;
  phone: string;
}


